import React from 'react';
import reactDOM from 'react-dom';
import List from './components/List.jsx';

reactDOM.render(<List />, document.getElementById('ingredients'));
