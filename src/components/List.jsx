import React from 'react';
import ListItem from './ListItem.jsx';

let ingredients = [{'id':1, 'text': 'ham'}, {'id':2, 'text': 'cheese'}, {'id':3, 'text': 'chicken'}]

export default class List extends React.Component {
  constructor(props) {
    super(props);
}
  render() {
    let listItems = ingredients.map((item) => {
      return <ListItem key={item.id} ingredient={item.text} />;
    });
    return (
      <ul>{listItems}</ul>
    )
  }
}
